def main(file):   
  with open("message.txt", "r") as file:
    if file.readlines() == ['\n']:
      MenuPrincipal1()
    elif file.readlines() != []:
        s.show_message("Un message est deja enregistre ", scroll_speed = 0.08, text_colour = Y)
        s.show_message("Supprimer?", scroll_speed = 0.1, text_colour = Y)
        
        #FCT CHOICE A IMPLEMENTER (option)
        avoid_release = True
        choice = True
        s.set_pixels(yes_button())
        event = s.stick.wait_for_event()
        while event.direction != "middle":            
          if avoid_release:
            avoid_release = False
            if event.direction == "up" or event.direction == "down":
              if choice:
                s.set_pixels(no_button())
                choice = False
              else:
                s.set_pixels(yes_button())
                choice = True
          else:
            avoid_release = True
            
          event = s.stick.wait_for_event()
          #FIN FCT CHOICE
          
          
        if not choice:
          #Mettre la clé et si correcte afficher le message
          cleinput = cle()  
          ligne = file.readlines()
          ligne = ligne[0]
          ligne = ligne.split()
          message_encode = ligne[3]
          cles_crypte = ligne[-1]
          cles_crypte = cles_crypte.split(":")
          separateur = ""
          cles_crypte = separateur.join(cles_crypte)
          cle_hachee = hashing(cleinput)
          if cle_hachee == cles_crypte:
            global fin
            fin = True
            messageDecode = decode(cleinput, message_encode)
            separateur2 = ""
            messageDecode = list(messageDecode)
            messageDecode = messageDecode[0: -1]
            messageDecode = separateur2.join(messageDecode)
            s.show_message("Le message est : " + messageDecode, text_colour = Y)
          else:
            s.show_message("code incorrecte", text_colour = R)
            fin = True
            
          
        elif choice:
          #Delete le contenu du fichier
          #Retour menu départ
          MenuPrincipal2()