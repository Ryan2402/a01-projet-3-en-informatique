#Imports
from sense_hat import SenseHat
from crypto import *
from file import *
import time
from cle import *
import random

#Fonctions et variables "globales"

s = SenseHat()
s.low_light = True

G = (0, 255, 0)
Y = (255, 255, 0)
B = (0, 0, 255)
R = (255, 0, 0)
W = (255,255,255)
N = (0,0,0)
P = (255,105, 180)

def chiffres():
  return [[
          N, N, N, N, N, N, N, N,
          N, N, Y, Y, Y, Y, N, N,
          N, N, Y, N, N, Y, N, N,
          N, N, Y, N, N, Y, N, N,
          N, N, Y, N, N, Y, N, N,
          N, N, Y, N, N, Y, N, N,
          N, N, Y, Y, Y, Y, N, N,
          N, N, N, N, N, N, N, N,
          ],[
          N, N, N, N, N, N, N, N,
          N, N, N, N, Y, N, N, N,
          N, N, N, Y, Y, N, N, N,
          N, N, N, N, Y, N, N, N,
          N, N, N, N, Y, N, N, N,
          N, N, N, N, Y, N, N, N,
          N, N, N, Y, Y, Y, N, N,
          N, N, N, N, N, N, N, N,
          ],
          [
          N, N, N, N, N, N, N, N,
          N, N, N, Y, Y, N, N, N,
          N, N, Y, N, N, Y, N, N,
          N, N, N, N, N, Y, N, N,
          N, N, N, N, Y, N, N, N,
          N, N, N, Y, N, N, N, N,
          N, N, Y, Y, Y, Y, N, N,
          N, N, N, N, N, N, N, N,
          ],
          [
          N, N, N, N, N, N, N, N,
          N, N, N, Y, Y, Y, N, N,
          N, N, Y, N, N, N, Y, N,
          N, N, N, N, N, N, Y, N,
          N, N, N, N, Y, Y, N, N,
          N, N, N, N, N, N, Y, N,
          N, N, Y, N, N, N, Y, N,
          N, N, N, Y, Y, Y, N, N,
          ],
          [
          N, N, N, N, N, N, N, N,
          N, N, Y, N, N, Y, N, N,
          N, N, Y, N, N, Y, N, N,
          N, N, Y, N, N, Y, N, N,
          N, N, Y, Y, Y, Y, N, N,
          N, N, N, N, N, Y, N, N,
          N, N, N, N, N, Y, N, N,
          N, N, N, N, N, N, N, N,
          ],
          [
          N, N, N, N, N, N, N, N,
          N, N, Y, Y, Y, Y, N, N,
          N, N, Y, N, N, N, N, N,
          N, N, Y, N, N, N, N, N,
          N, N, Y, Y, Y, Y, N, N,
          N, N, N, N, N, Y, N, N,
          N, N, Y, Y, Y, Y, N, N,
          N, N, N, N, N, N, N, N,
          ],
          [
          N, N, N, N, N, N, N, N,
          N, N, Y, Y, Y, Y, N, N,
          N, N, Y, N, N, N, N, N,
          N, N, Y, N, N, N, N, N,
          N, N, Y, Y, Y, Y, N, N,
          N, N, Y, N, N, Y, N, N,
          N, N, Y, Y, Y, Y, N, N,
          N, N, N, N, N, N, N, N,
          ],[
          N, N, N, N, N, N, N, N,
          N, N, Y, Y, Y, Y, N, N,
          N, N, N, N, N, Y, N, N,
          N, N, N, N, N, Y, N, N,
          N, N, N, N, N, Y, N, N,
          N, N, N, N, N, Y, N, N,
          N, N, N, N, N, Y, N, N,
          N, N, N, N, N, N, N, N,
          ],
          [
          N, N, N, N, N, N, N, N,
          N, N, Y, Y, Y, Y, N, N,
          N, N, Y, N, N, Y, N, N,
          N, N, Y, Y, Y, Y, N, N,
          N, N, Y, N, N, Y, N, N,
          N, N, Y, N, N, Y, N, N,
          N, N, Y, Y, Y, Y, N, N,
          N, N, N, N, N, N, N, N,
          ],[
          N, N, N, N, N, N, N, N,
          N, N, Y, Y, Y, Y, N, N,
          N, N, Y, N, N, Y, N, N,
          N, N, Y, N, N, Y, N, N,
          N, N, Y, Y, Y, Y, N, N,
          N, N, N, N, N, Y, N, N,
          N, N, Y, Y, Y, Y, N, N,
          N, N, N, N, N, N, N, N,
          ]
          ]
        
def MenuPrincipal1():   #Cas où file vide
    avoid_release = True
    s.show_message("Entrez un message", scroll_speed = 0.1, text_colour = Y)
    numbers = []
    flag = True
    not_down = True
    while not_down:
      current_number = 0
      s.set_pixels(chiffres()[current_number])
      
      event = s.stick.wait_for_event()
      while event.direction != "middle" and not_down:
        if avoid_release:
          avoid_release = False
          if event.direction == "right":
            if current_number < 9:
              current_number+= 1
            else:
              current_number = 0
          elif event.direction == "left":
            if current_number > 0:
              current_number-= 1
            else:
              current_number = 9
          elif event.direction =="down": # à régler
            not_down = False
            
          s.set_pixels(chiffres()[current_number])
        else:
          avoid_release = True
          
        event = s.stick.wait_for_event()
        
      if flag:
        flag = False
        if not_down :
          numbers.append(current_number)
      else:
        flag = True

    string = "Votre message est "
    message = ""            #Pour utiliser la fct encode()
    
    for i in range (len(numbers)):
      string += str(numbers[i])
      message += str(numbers[i])

    s.show_message(string, scroll_speed = 0.1, text_colour = Y)
    
    cles = cle()
    message = ""
    for i in range (len(numbers)):
      message +=  str(numbers[i])
    message_code = encode(cles, str(message))
    cles = hashing(cles)
    delete("message.txt")
    append("message.txt", "Votre message est: {}, Votre code/clé est : {}".format(str(message_code), cles))
    global fin
    fin = True
    
def MenuPrincipal2():   #Cas où file PLEIN
    avoid_release = True
    s.show_message("Entrez un message", scroll_speed = 0.1, text_colour = Y)
    numbers = []
    flag = False
    not_down = True
    while not_down:
      current_number = 0
      s.set_pixels(chiffres()[current_number])
      
      event = s.stick.wait_for_event()
      while event.direction != "middle" and not_down:
        if avoid_release:
          avoid_release = False
          if event.direction == "right":
            if current_number < 9:
              current_number+= 1
            else:
              current_number = 0
          elif event.direction == "left":
            if current_number > 0:
              current_number-= 1
            else:
              current_number = 9
              
          elif event.direction == "down":
            not_down = False
            
          s.set_pixels(chiffres()[current_number])
        else:
          avoid_release = True
          
        event = s.stick.wait_for_event()
        
      if flag:
        flag = False
        if not_down:
          numbers.append(current_number)
      else:
        flag = True
    string = "Votre message est "

    for i in range (len(numbers)):
      string+= str(numbers[i])
      

    s.show_message(string, scroll_speed = 0.1, text_colour = Y)
    global fin
    fin = True
    cles = cle()
    if exists("message.txt"):
      message = ""
      for i in range (len(numbers)):
        message +=  str(numbers[i])
      message_code = encode(cles, str(message))
      cles = hashing(cles)
      delete("message.txt")
      append("message.txt", "Votre message est: {}, Votre clé est : {}".format(str(message_code), cles))
    else:
      s.show_message("le fichier n'existe pas", text_colour = R)
    
def yes_button():
  yes = [
    N, W, W, W, W, W, W, N,
    N, W, N, N, N, G, W, N,
    N, W, G, N, G, N, W, N,
    N, W, N, G, N, N, W, N,
    N, W, W, W, W, W, W, N,
    N, N, R, N, N, R, N, N,
    N, N, N, R, R, N, N, N,
    N, N, R, N, N, R, N, N,
    ]
  return yes
  
def no_button():
  no = [
    N, N, N, N, N, G, N, N,
    N, N, G, N, G, N, N, N,
    N, N, N, G, N, N, N, N,
    N, W, W, W, W, W, W, N,
    N, W, R, N, N, R, W, N,
    N, W, N, R, R, N, W, N,
    N, W, R, N, N, R, W, N,
    N, W, W, W, W, W, W, N,
    ]
  return no

##########
#FONCTIONS DU JEU
#########
black_screen = [N, N, N, N, N, N, N, N,
N, N, N, N, N, N, N, N,
N, N, N, N, N, N, N, N,
N, N, N, N, N, N, N, N,
N, N, N, N, N, N, N, N,
N, N, N, N, N, N, N, N,
N, N, N, N, N, N, N, N,
N, N, N, N, N, N, N, N,
]

pomme_x = 0
pomme_y = 0
colonnes = [0, 1, 2, 3, 4, 5, 6, 7]
panier_x = len(colonnes) // 2
panier_y = 7
isGameOver = False
score = 0

def pomme():
  global pomme_x
  global pomme_y
  pomme_y = 0
  pomme_x = random.choice(colonnes)
  s.set_pixel(pomme_x, pomme_y, G)
        
def falling():
  global pomme_x
  global pomme_y
  global panier_x
  global colonnes
  global interval
  global score
  
  if pomme_y < len(colonnes) - 1:
    s.set_pixel(pomme_x, pomme_y, N)
    pomme_y = pomme_y + 1
    s.set_pixel(pomme_x, pomme_y, G)
    panier()
  elif pomme_x == panier_x:
    score = score + 1
    panier()
    pomme()
    interval = interval - 0.1
  else:
    gameOver()

def panier():
  global panier_x
  global panier_y
  s.set_pixel(panier_x, panier_y, P)

def gameOver():
  global isGameOver
  global score
  isGameOver = True

def main(file):   
  with open("message.txt", "r") as file:
    if file.readlines() == ['\n']:
      MenuPrincipal1()
    elif file.readlines() != []:
        s.show_message("Un message est deja enregistre ", scroll_speed = 0.08, text_colour = Y)
        s.show_message("Supprimer?", scroll_speed = 0.1, text_colour = Y)
        
        #FCT CHOICE A IMPLEMENTER (option)
        avoid_release = True
        choice = True
        s.set_pixels(yes_button())
        event = s.stick.wait_for_event()
        while event.direction != "middle":            
          if avoid_release:
            avoid_release = False
            if event.direction == "up" or event.direction == "down":
              if choice:
                s.set_pixels(no_button())
                choice = False
              else:
                s.set_pixels(yes_button())
                choice = True
          else:
            avoid_release = True
            
          event = s.stick.wait_for_event()
          #FIN FCT CHOICE
          
          
        if not choice:
          #Mettre la clé et si correcte afficher le message
          cleinput = cle()  
          ligne = file.readlines()
          ligne = ligne[0]
          ligne = ligne.split()
          message_encode = ligne[3]
          cles_crypte = ligne[-1]
          cles_crypte = cles_crypte.split(":")
          separateur = ""
          cles_crypte = separateur.join(cles_crypte)
          cle_hachee = hashing(cleinput)
          if cle_hachee == cles_crypte:
            global fin
            fin = True
            messageDecode = decode(cleinput, message_encode)
            separateur2 = ""
            messageDecode = list(messageDecode)
            messageDecode = messageDecode[0: -1]
            messageDecode = separateur2.join(messageDecode)
            s.show_message("Le message est : " + messageDecode, text_colour = Y)
          else:
            s.show_message("code incorrecte", text_colour = R)
            fin = True
            
          
        elif choice:
          #Delete le contenu du fichier
          #Retour menu départ
          MenuPrincipal2()

#Lancement du jeu
interval = 1
lastMove = time.time()
easter_egg2 = []
s.show_message("Bienvenue dans le jeu 1h04", scroll_speed = 0.08, text_colour = Y)
s.set_pixels(black_screen)
pomme()
panier()
while not isGameOver: #boucle du jeu
  fin = False
  currentTimestamp = time.time()
  if (currentTimestamp - lastMove >= interval):
    lastMove = currentTimestamp
    falling()
  events = s.stick.get_events()
  if events:
    for e in events:
      if (e.direction == "left" or e.direction == "right") and e.action == "pressed":
        s.set_pixel(panier_x, panier_y, N)
        if e.direction == "left" and panier_x > 0:
          panier_x = panier_x - 1
        elif e.direction == "right" and panier_x < len(colonnes) - 1:
          panier_x = panier_x + 1
        panier()
      easter_egg =["up","up","up","up"] # code à réaliser pour l'easter-egg
      if score == 1: # autre condition pour l'easter-egg
        if e.direction == "up" and e.action == "pressed":
          easter_egg2.append("up")
        if easter_egg == easter_egg2: #entrer dans l'easter-egg
          isGameOver = True
          s.show_message("Magiclock", scroll_speed = 0.1, text_colour = Y)
          file = open("message.txt", "r")
          main(file)

if fin:
  s.show_message("Fin du Magiclock")
  
else:
  for i in range(100): # boucle permettant le "try again"
    s.set_pixels(black_screen)
    pomme()
    panier()
    if isGameOver:
      s.show_message("Perdu !", scroll_speed = 0.1, text_colour = R)
      s.show_message("Score: " + str(score), scroll_speed = 0.1, text_colour = G)
      s.show_message("Rejouer ?", scroll_speed = 0.1, text_colour = G)
      
      avoid_release = True
      choice = True
      s.set_pixels(yes_button())
      event = s.stick.wait_for_event()
      while event.direction != "middle":            
        if avoid_release:
          avoid_release = False
          if event.direction == "up" or event.direction == "down":
            if choice:
              s.set_pixels(no_button())
              choice = False
            else:
              s.set_pixels(yes_button())
              choice = True
        else:
          avoid_release = True
          
        event = s.stick.wait_for_event()
      if choice: # On recommence le jeu
        isGameOver = False
        s.set_pixels(black_screen)
        score = 0
        interval = 1
        while not isGameOver:
          currentTimestamp = time.time()
          
          if (currentTimestamp - lastMove >= interval):
            lastMove = currentTimestamp
            falling()
        
          events = s.stick.get_events()
          if events:
            for e in events:
              if (e.direction == "left" or e.direction == "right") and e.action == "pressed":
                s.set_pixel(panier_x, panier_y, N)
                if e.direction == "left" and panier_x > 0:
                  panier_x = panier_x - 1
                elif e.direction == "right" and panier_x < len(colonnes) - 1:
                  panier_x = panier_x + 1
                panier()
              easter_egg =["up","up","up","up"] # code à réaliser pour l'easter-egg
              if score == 1: # autre condition pour l'easter-egg
                if e.direction == "up" and e.action == "pressed":
                  easter_egg2.append("up")
                if easter_egg == easter_egg2: #entrer dans l'easter-egg
                  isGameOver = True
                  s.show_message("Magiclock", scroll_speed = 0.1, text_colour = Y)
                  main(file)
      else: # fin du programme
        s.show_message("Merci d'avoir joue !", scroll_speed = 0.1, text_colour = Y)
        break
        


        
        







