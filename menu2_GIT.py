def MenuPrincipal2():   #Cas où file PLEIN
    avoid_release = True
    s.show_message("Entrez un message", scroll_speed = 0.1, text_colour = Y)
    numbers = []
    flag = False
    not_down = True
    while not_down:
      current_number = 0
      s.set_pixels(chiffres()[current_number])
      
      event = s.stick.wait_for_event()
      while event.direction != "middle" and not_down:
        if avoid_release:
          avoid_release = False
          if event.direction == "right":
            if current_number < 9:
              current_number+= 1
            else:
              current_number = 0
          elif event.direction == "left":
            if current_number > 0:
              current_number-= 1
            else:
              current_number = 9
              
          elif event.direction == "down":
            not_down = False
            
          s.set_pixels(chiffres()[current_number])
        else:
          avoid_release = True
          
        event = s.stick.wait_for_event()
        
      if flag:
        flag = False
        if not_down:
          numbers.append(current_number)
      else:
        flag = True
    string = "Votre message est "

    for i in range (len(numbers)):
      string+= str(numbers[i])
      

    s.show_message(string, scroll_speed = 0.1, text_colour = Y)
    global fin
    fin = True
    cles = cle()
    if exists("message.txt"):
      message = ""
      for i in range (len(numbers)):
        message +=  str(numbers[i])
      message_code = encode(cles, str(message))
      cles = hashing(cles)
      delete("message.txt")
      append("message.txt", "Votre message est: {}, Votre clé est : {}".format(str(message_code), cles))
    else:
      s.show_message("le fichier n'existe pas", text_colour = R)