 #FCT CHOICE A IMPLEMENTER (option)
avoid_release = True
choice = True
s.set_pixels(yes_button())
event = s.stick.wait_for_event()
while event.direction != "middle":            
  if avoid_release:
    avoid_release = False
    if event.direction == "up" or event.direction == "down":
      if choice:
        s.set_pixels(no_button())
        choice = False
      else:
        s.set_pixels(yes_button())
        choice = True
  else:
    avoid_release = True
    
  event = s.stick.wait_for_event()
  #FIN FCT CHOICE
