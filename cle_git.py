from sense_hat import SenseHat
def cle():
#from alphabet import *

    s = SenseHat()
    s.low_light = True
    Y = (255, 255, 0)
 
    cle = ""
    x = 0
    s.show_message("Entrez un code", scroll_speed = 0.1, text_colour = Y)
    while x == 0:
      orientation = s.get_orientation_degrees()
      pitch = orientation["pitch"]
      roll = orientation["roll"]
      yaw = orientation["yaw"]

      if 350.0 < pitch or pitch < 10.0:
        if 350.0 < roll or roll < 10.0:
          if 350.0 < yaw or yaw < 10.0:
            s.show_message("0", text_colour = Y)
            choix = "0"
          elif 80.0 < yaw < 100.0:
            s.show_message("1", text_colour = Y)
            choix = "1"
          elif 170.0 < yaw < 190.0:
            s.show_message("2", text_colour = Y)
            choix = "2"
          elif 260.0 < yaw < 280.0:
            s.show_message("3", text_colour = Y)
            choix = "3"
        elif 80.0 < roll < 100.0:
          if 350.0 < yaw or yaw < 10.0:
            s.show_message("4", text_colour = Y)
            choix = "4"
          elif 80.0 < yaw < 100.0:
            s.show_message("5", text_colour = Y)
            choix = "5"
          elif 170.0 < yaw < 190.0:
            s.show_message("6", text_colour = Y)
            choix = "6"
          elif 260.0 < yaw < 280.0:
            s.show_message("7", text_colour = Y)
            choix = "7"
        elif 170.0 < roll < 190.0:
          if 350.0 < yaw or yaw < 10.0:
            s.show_message("8", text_colour = Y)
            choix = "8"
          elif 80.0 < yaw < 100.0:
            s.show_message("9", text_colour = Y)
            choix = "9"
          elif 170.0 < yaw < 190.0:
            s.show_message("10", text_colour = Y)
            choix = "10"
          elif 260.0 < yaw < 280.0:
            s.show_message("11", text_colour = Y)
            choix = "11"
        elif 260.0 < roll < 280.0:
          if 350.0 < yaw or yaw < 10.0:
            s.show_message("12", text_colour = Y)
            choix = "12"
          elif 80.0 < yaw < 100.0:
            s.show_message("13", text_colour = Y)
            choix = "14"
          elif 170.0 < yaw < 190.0:
            s.show_message("15", text_colour = Y)
            choix = "15"
          elif 260.0 < yaw < 280.0:
            s.show_message("16", text_colour = Y)
            choix = "16"
      elif 80.0 < pitch < 100.0:
        if 350.0 < roll or roll < 10.0:
          if 350.0 < yaw or yaw < 10.0:
            s.show_message("17", text_colour = Y)
            choix = "17"
          elif 80.0 < yaw < 100.0:
            s.show_message("18", text_colour = Y)
            choix = "18"
          elif 260.0 < yaw < 280.0:
            s.show_message("19", text_colour = Y)
            choix = "19"
        elif 80.0 < roll < 100.0:
          if 350.0 < yaw or yaw < 10.0:
            s.show_message("20", text_colour = Y)
            choix = "20"
          elif 80.0 < yaw < 100.0:
            s.show_message("21", text_colour = Y)
            choix = "21"
          elif 170.0 < yaw < 190.0:
            s.show_message("22", text_colour = Y)
            choix = "22"
          elif 260.0 < yaw < 280.0:
            s.show_message("23", text_colour = Y)
            choix = "23"
        elif 170.0 < roll < 190.0:
          if 350.0 < yaw or yaw < 10.0:
            s.show_message("24", text_colour = Y)
            choix = "24"
          elif 80.0 < yaw < 100.0:
            s.show_message("25", text_colour = Y)
            choix = "25"
          elif 170.0 < yaw < 190.0:
            s.show_message("26", text_colour = Y)
            choix = "26"
          elif 260.0 < yaw < 280.0:
            s.show_message("27", text_colour = Y)
            choix = "27"
        elif 260.0 < roll < 280.0:
          if 350.0 < yaw or yaw < 10.0:
            s.show_message("28", text_colour = Y)
            choix = "28"
          elif 80.0 < yaw < 100.0:
            s.show_message("29", text_colour = Y)
            choix = "29"
          elif 170.0 < yaw < 190.0:
            s.show_message("30", text_colour = Y)
            choix = "30"
          elif 260.0 < yaw < 280.0:
            s.show_message("31", text_colour = Y)
            choix = "31"
      elif 170.0 < pitch < 190.0:
        if 350.0 < roll or roll < 10.0:
          if 350.0 < yaw or yaw < 10.0:
            s.show_message("32", text_colour = Y)
            choix = "32"
          elif 80.0 < yaw < 100.0:
            s.show_message("33", text_colour = Y)
            choix = "33"
          elif 170.0 < yaw < 190.0:
            s.show_message("34", text_colour = Y)
            choix = "34"
          elif 260.0 < yaw < 280.0:
            s.show_message("35", text_colour = Y)
            choix = "35"
        elif 80.0 < roll < 100.0:
          if 350.0 < yaw or yaw < 10.0:
            s.show_message("36", text_colour = Y)
            choix = "36"
          elif 80.0 < yaw < 100.0:
            s.show_message("37", text_colour = Y)
            choix = "37"
          elif 170.0 < yaw < 190.0:
            s.show_message("38", text_colour = Y)
            choix = "38"
          elif 260.0 < yaw < 280.0:
            s.show_message("39", text_colour = Y)
            choix = "39"
        elif 170.0 < roll < 190.0:
          if 350.0 < yaw or yaw < 10.0:
            s.show_message("40", text_colour = Y)
            choix = "40"
          elif 80.0 < yaw < 100.0:
            s.show_message("41", text_colour = Y)
            choix = "41"
          elif 170.0 < yaw < 190.0:
            s.show_message("42", text_colour = Y)
            choix = "42"
          elif 260.0 < yaw < 280.0:
            s.show_message("43", text_colour = Y)
            choix = "43"
        elif 260.0 < roll < 280.0:
          if 350.0 < yaw or yaw < 10.0:
            s.show_message("44", text_colour = Y)
            choix = "44"
          elif 80.0 < yaw < 100.0:
            s.show_message("45", text_colour = Y)
            choix = "45"
          elif 170.0 < yaw < 190.0:
            s.show_message("46", text_colour = Y)
            choix = "46"
          elif 260.0 < yaw < 280.0:
            s.show_message("47", text_colour = Y)
            choix = "47"
                
      elif pitch == 270.0:
        if 350.0 < roll or roll < 10.0:
          if 350.0 < yaw or yaw < 10.0:
            s.show_message("48", text_colour = Y)
            choix = "48"
          elif 80.0 < yaw < 100.0:
            s.show_message("49", text_colour = Y)
            choix = "49"
          elif 170.0 < yaw < 190.0:
            s.show_message("50", text_colour = Y)
            choix = "50"
          elif 260.0 < yaw < 280.0:
            s.show_message("51", text_colour = Y)
            choix = "51"
        elif 80.0 < roll < 100.0:
          if 350.0 < yaw or yaw < 10.0:
            s.show_message("52", text_colour = Y)
            choix = "52"
          elif 80.0 < yaw < 100.0:
            s.show_message("53", text_colour = Y)
            choix = "53"
          elif 170.0 < yaw < 190.0:
            s.show_message("54", text_colour = Y)
            choix = "54"
          elif 260.0 < yaw < 280.0:
            s.show_message("55", text_colour = Y)
            choix = "55"
        elif 170.0 < roll < 190.0:
          if 350.0 < yaw or yaw < 10.0:
            s.show_message("56", text_colour = Y)
            choix = "56"
          elif 80.0 < yaw < 100.0:
            s.show_message("57", text_colour = Y)
            choix = "57"
          elif 170.0 < yaw < 190.0:
            s.show_message("58", text_colour = Y)
            choix = "58"
          elif 260.0 < yaw < 280.0:
            s.show_message("59", text_colour = Y)
            choix = "59"
        elif 260.0 < roll < 280.0:
          if 350.0 < yaw or yaw < 10.0:
            s.show_message("60", text_colour = Y)
            choix = "60"
          elif 80.0 < yaw < 100.0:
            s.show_message("61", text_colour = Y)
            choix = "61"
          elif 170.0 < yaw < 190.0:
            s.show_message("62", text_colour = Y)
            choix = "62"
          elif 260.0 < yaw < 280.0:
            s.show_message("63", text_colour = Y)
            choix = "63"
      for event in s.stick.get_events():
        if event.direction == "middle" and event.action == "pressed":
          cle = cle + choix + " "
        elif event.direction == "right":
          x = 1
    s.show_message("Votre code est: " + cle,scroll_speed = 0.1, text_colour = Y)
    return cle