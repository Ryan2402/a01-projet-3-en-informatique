pomme_x = 0
pomme_y = 0
colonnes = [0, 1, 2, 3, 4, 5, 6, 7]
panier_x = len(colonnes) // 2
panier_y = 7
isGameOver = False
score = 0

def pomme():
  global pomme_x
  global pomme_y
  pomme_y = 0
  pomme_x = random.choice(colonnes)
  s.set_pixel(pomme_x, pomme_y, G)
        
def falling():
  global pomme_x
  global pomme_y
  global panier_x
  global colonnes
  global interval
  global score
  
  if pomme_y < len(colonnes) - 1:
    s.set_pixel(pomme_x, pomme_y, N)
    pomme_y = pomme_y + 1
    s.set_pixel(pomme_x, pomme_y, G)
    panier()
  elif pomme_x == panier_x:
    score = score + 1
    panier()
    pomme()
    interval = interval - 0.1
  else:
    gameOver()

def panier():
  global panier_x
  global panier_y
  s.set_pixel(panier_x, panier_y, P)

def gameOver():
  global isGameOver
  global score
  isGameOver = True

interval = 1
lastMove = time.time()
easter_egg2 = []
s.show_message("Bienvenue dans le jeu 1h04", scroll_speed = 0.08, text_colour = Y)
s.set_pixels(black_screen)
pomme()
panier()
while not isGameOver: #boucle du jeu
  fin = False
  currentTimestamp = time.time()
  if (currentTimestamp - lastMove >= interval):
    lastMove = currentTimestamp
    falling()
  events = s.stick.get_events()
  if events:
    for e in events:
      if (e.direction == "left" or e.direction == "right") and e.action == "pressed":
        s.set_pixel(panier_x, panier_y, N)
        if e.direction == "left" and panier_x > 0:
          panier_x = panier_x - 1
        elif e.direction == "right" and panier_x < len(colonnes) - 1:
          panier_x = panier_x + 1
        panier()
      easter_egg =["up","up","up","up"] # code à réaliser pour l'easter-egg
      if score == 1: # autre condition pour l'easter-egg
        if e.direction == "up" and e.action == "pressed":
          easter_egg2.append("up")
        if easter_egg == easter_egg2: #entrer dans l'easter-egg
          isGameOver = True
          s.show_message("Magiclock", scroll_speed = 0.1, text_colour = Y)
          file = open("message.txt", "r")
          main(file)

if fin:
  s.show_message("Fin du Magiclock")
  
else:
  for i in range(100): # boucle permettant le "try again"
    s.set_pixels(black_screen)
    pomme()
    panier()
    if isGameOver:
      s.show_message("Perdu !", scroll_speed = 0.1, text_colour = R)
      s.show_message("Score: " + str(score), scroll_speed = 0.1, text_colour = G)
      s.show_message("Rejouer ?", scroll_speed = 0.1, text_colour = G)
      
      avoid_release = True
      choice = True
      s.set_pixels(yes_button())
      event = s.stick.wait_for_event()
      while event.direction != "middle":            
        if avoid_release:
          avoid_release = False
          if event.direction == "up" or event.direction == "down":
            if choice:
              s.set_pixels(no_button())
              choice = False
            else:
              s.set_pixels(yes_button())
              choice = True
        else:
          avoid_release = True
          
        event = s.stick.wait_for_event()
      if choice: # On recommence le jeu
        isGameOver = False
        s.set_pixels(black_screen)
        score = 0
        interval = 1
        while not isGameOver:
          currentTimestamp = time.time()
          
          if (currentTimestamp - lastMove >= interval):
            lastMove = currentTimestamp
            falling()
        
          events = s.stick.get_events()
          if events:
            for e in events:
              if (e.direction == "left" or e.direction == "right") and e.action == "pressed":
                s.set_pixel(panier_x, panier_y, N)
                if e.direction == "left" and panier_x > 0:
                  panier_x = panier_x - 1
                elif e.direction == "right" and panier_x < len(colonnes) - 1:
                  panier_x = panier_x + 1
                panier()
              easter_egg =["up","up","up","up"] # code à réaliser pour l'easter-egg
              if score == 1: # autre condition pour l'easter-egg
                if e.direction == "up" and e.action == "pressed":
                  easter_egg2.append("up")
                if easter_egg == easter_egg2: #entrer dans l'easter-egg
                  isGameOver = True
                  s.show_message("Magiclock", scroll_speed = 0.1, text_colour = Y)
                  main(file)
      else: # fin du programme
        s.show_message("Merci d'avoir joue !", scroll_speed = 0.1, text_colour = Y)
        break